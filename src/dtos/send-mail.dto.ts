import {IsString} from "class-validator";

export class SendMailDto {
  @IsString()
  someProperty: string;

  mail: {
    subject: string;
    htmlBody: string;
  };
  addresses: {
    to: string[];
    cc?: string[];
  };
  sender: {
    name: string;
    email: string;
  };
}

